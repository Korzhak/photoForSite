void RegWrite(int val, int pinSH_CP, int pinDS, int pinST_CP, int numb)
{
    PORTB &= ~(1<<pinST_CP); // ST_CP = 0;
    for (int i = (numb - 1); i >= 0; i--)
    {
        PORTB|=( ((val>>i)&0x01)<<pinDS); // DS = value << i
        PORTB|=(1<<pinSH_CP); // SH_CP = 1
        PORTB&=~(1<<pinSH_CP); // SH_CP = 0;
        PORTB&=~(1<<1); // DS = 0
    }
    PORTB|=(1<<2); //ST_CP = 1;
}
